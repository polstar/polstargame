# PolStarGame

Interface et moteur du jeu PolStar, inspiré par la série de romans de science-fiction "Étoiles perdues" de Jack Campbell.

## Développement

### Éléments techniques

Le jeu est développé en Javascript et s'appuie sur les technologies Web associées, pour assurer une pprtabilité sur toutes les plateformes existantes : ordinateurs personnels, tablettes, smartphones.

Il est prévu pour fonctionner hors-ligne, en metant en cache tous les éléemtns du jeu, permettant d'y jouer sans connexion Internet active.

Plusieurs outils sont nécessaires à son développement et à sa construction à partir des sources : Node JS (structure Javascript pour la machine du développeur), Gulp (build, génération du fichier de cache), EaselJS (partie 2D du jeu, dans le canvas d'HTML5), internationalisation, Less (écriture avancée des styles).

Des tutoriels m'ont permis de choisir puis maîtriser ces outils :

- https://www.alsacreations.com/tuto/lire/1686-introduction-a-gulp.html

- https://gulpjs.com/docs/en/getting-started/quick-start/

- https://www.html5rocks.com/en/tutorials/appcache/beginner/

- https://github.com/hillmanov/gulp-manifest

- https://www.createjs.com/getting-started/easeljs

- https://developer.mozilla.org/fr/docs/Mozilla/Add-ons/WebExtensions/Internationalization

- http://lesscss.org/

- https://eloquentjavascript.net/10_modules.html

### Éléments incorporés

Une partie des bibliothèques doivent être chargées et incluses dans les fichiers distribués (build) et être mis dans le cache du navigateur chez l'utilisateur.

Pour ce faire, il convient d'inclure ces bibliothèques dans le fichier `package.json`, afin qu'elles soient traitées automatiquement via un `npm install` à la racine du projet.

NB : les fichiers Less (styles) seront compilés avant la construction des fichiers à distribuer, afin que le jeu soit bien mis en cache (à la différence de ce qui se fait habituellement, avec une référence à Less dans les headers HTML).

### Machine de développement

Pour regénérer (build) les fichiers du jeu, après par exemple une traduction dans votre langue, il vous faut cloner le projet, puis installer les bibliothèques utilisées, et enfin faire un `build`, ce qui suppose de préparer votre ordinateur.

Je vous conseille d'installer les outils (Node JS, npm, node-less et Gulp) via les mécanismes habituels de votre système d'exploitation, pour éviter la multiplication des répertoires `node_modules` intempestifs et mal placés (surtout si l'ordinateur est utilisé par plusieurs DEVs ou pour des projets à différents endroits de la machine).

### Construction du jeu

Il vous faut procéder aux étpaes suivantes :

- cloner le projet depuis GitLab : https://gitlab.com/polstar/polstargame

- installer les dépendances : `npm install` à la racine du projet

- générer le jeu : `gulp build`

Vous aurez les fichiers du jeu dans le répertoire `build` du projet
